package Trajectoire;


import Observable.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class Traject {

  private int x, y;
  public BufferedImage image;
  private int[][] pixel;
  private LinkedList<Point> line = new LinkedList<Point>();

  /*
   * Cette fonction , va nous permettre d'analyser l'image de la trajectoire .
   * Ceci , va nous permettre de savoir les limites d'une trajectoire
   */
  public Traject(int x, int y, BufferedImage image) {
    x = x;
    y = y;
    image = image;
  }

  public int getPosiX() {
    return x;
  }

  public int getPosiY() {
    return y;
  }


  public void setPosition(int x, int y) {
    x = x;
    y = y;
  }

  public Image getImage() {
    return image;
  }

  public int[][] getpixel() {
    return pixel;
  }

  public boolean isOut(LinkedList<Point> cadre) {

    for (Point point : cadre) {

      if (isTransparence(point.returnX(), point.returnY())) {
        return true;

      }
    }
    return false;
  }

  public boolean isTransparence(int x, int y) {
    if (x < 0) {
      return true;
    }
    if (y < 0) {
      return true;
    }

    if (x >= image.getWidth()) {
      return true;
    }
    if (y >= image.getHeight()) {
      return true;
    }

    int pix = ((BufferedImage) image).getRGB(x, y);

    int alpha = (pix >> 24) & 0xFF;

    if (alpha < 100) {

      return true;
    }
    return false;
  }

  public boolean isWhite(int x, int y) {
    if (x < 0) {
      return true;
    }
    if (y < 0) {
      return true;
    }

    if (x >= image.getWidth()) {
      return true;
    }
    if (y >= image.getHeight()) {
      return true;
    }

    Color color = new Color(((BufferedImage) image).getRGB(x, y));

    if (color.equals(Color.WHITE)) {

      return true;
    }
    return false;
  }

  public int extremiste(int x, int y, double rotation) {

    int i = x;
    int j = y;
    int k = x;
    int l = y;
    int d = 10;

    while (!isTransparence(i, j)) {
      i = i + (int) (d * Math.sin(rotation));
      j = j - (int) (d * Math.cos(rotation));
    }
    while (!isTransparence(k, l)) {
      k = k - (int) (d * Math.sin(rotation));
      l = l + (int) (d * Math.cos(rotation));
    }
    float x1 = (float) (Math.sqrt(Math.pow(i - x, 2) + Math.pow(j - y, 2))
        - Math.sqrt(Math.pow(k - x, 2) + Math.pow(l - y, 2)) / 0.9);
    line.push(new Point(i, j));
    line.push(new Point(k, l));
    System.out.print("\n" + x1);
    if (x1 < -500) {
      return 0;
    }
    if (-500 <= x1 && x1 < -100) {
      return 1;
    }
    if (-100 <= x1 && x1 < -75) {
      return 2;
    }
    if (-75 <= x1 && x1 < 75) {

      return 3;
    }
    if (75 <= x1 && x1 < 100) {
      return 4;
    }
    if (100 <= x1 && x1 < 500) {
      return 5;
    }
    if (x1 >= 500) {
      return 6;
    }

    return 7;
  }

  public int[] returnCadreX() {

    int[] cadreX = new int[line.size()];
    int i = 0;
    for (Point point : line) {
      cadreX[i] = point.returnX();
      i++;
    }
    return cadreX;
  }

  public int[] returnCadreY() {

    int[] cadreY = new int[line.size()];
    int i = 0;
    for (Point point : line) {
      cadreY[i] = point.returnY();
      i++;
    }
    return cadreY;
  }
}
