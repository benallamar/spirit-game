package Observateur;

import Observable.Observable;
import javax.swing.JPanel;


public class Paneau extends JPanel {

  /*
   * On essaye de dessiner notre voiture et notre trajectoire
   */
  private int time;
  private Observable observable;
  /* ce paneau va nous servir , pour dessiner notre chemin sur le panel
	 * 
	 */

  public void paintComponent(Graphics g) {
    Graphics2D g2D = (Graphics2D) g;

    try {

      g2D.drawImage(observable.getTraject().image, 0, 0, ;
      g2D.drawPolygon(observable.getTraject().returnCadreX(), observable.getTraject().returnCadreY(), 2);
      g2D.drawPolygon(observable.getCar(0).returnCadreX(), observable.getCar(0).returnCadreY(), 8);
      g2D.drawPolygon(observable.getCar(1).returnCadreX(), observable.getCar(1).returnCadreY(), 8);
      g2D.drawPolygon(observable.getCar(2).returnCadreX(), observable.getCar(2).returnCadreY(), 8);
      g2D.drawImage(observable.getCar(0).getImage(), observable.getCar(0).getTrans(), null);
      g2D.drawImage(observable.getCar(1).getImage(), observable.getCar(1).getTrans(), null);
      g2D.drawImage(observable.getCar(2).getImage(), observable.getCar(2).getTrans(), null);

    } catch (Exception e) {
      // TODO: handle exception
    }


  }

  public int getPosX(int i) {
    return observable.getCar(i).returnPosition()[0];
  }


  public int getPosY(int i) {
    return observable.getCar(i).returnPosition()[1];
  }

  /*
   * cette fonction va nous permettre � chaque instant de changer les cordones de notre voiture
   */
  public void setPos(int[] posi, int i) {
    observable.getCar(i).setPosition(posi[0], posi[1]);
  }

  public Paneau(Observable observable, int time) {
    observable = observable;
    time = time;
  }

  public void clean() {
    removeAll();
  }

  public int getTime() {
    return time;
  }

  public Observable getObservable() {
    return observable;
  }

}