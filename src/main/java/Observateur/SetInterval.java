package Observateur;

public class SetInterval {

  private int time;

  public SetInterval(int X) {
    time = X;
    setIntervale();
  }

  public void setIntervale() {
    try {
      Thread.sleep(time);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
