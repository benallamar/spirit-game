package Observateur;

import Controle.KeyEventControle;
import Observable.Observable;

/*
 * cette classe la va nous permettre 
 * d'afficher notre jeux sur un �cran de pc
 */
public class Observateur {

  private JFrame fenetre = new JFrame();
  private Observable observable;
  private Paneau pan;

  /*
   * constructeur de notre voiture
   */
  public Observateur(Observable observ) {
    pan = new Paneau(observ, 100);
    KeyEventControle controle = new KeyEventControle(pan);

    fenetre.setTitle("c'est notre jeux");
    fenetre.setSize(800, 1300);
    fenetre.setLocationRelativeTo(null);
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.addKeyListener(controle);

    fenetre.setFocusable(true);
    fenetre.setContentPane(pan);
    fenetre.setVisible(true);
    pan.repaint();
    go(controle);

  }

  /* Cette fonction va nous permettre � chaque instant de redessiner notre car , apres chaque
   * modification
   */
  public void go(KeyEventControle controle) {

    while (true) {

      pan.repaint();
      controle.essayBouger(1);
      controle.essayBouger(2);
      controle.essayBouger(3);

      SetInterval timer = new SetInterval(120);
    }
  }


}
