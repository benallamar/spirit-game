package Observable;

import Trajectoire.Traject;
import java.util.LinkedList;

public class Observable {

  private Car[] cars = new Car[4];
  private Traject traject;

  /*
   * consructeur de notre observable
   */
  public Observable(LinkedList<Car> _cars, Traject traject) {
    for (int i = 0; i < 4; i++) {
      cars[i] = _cars.pop();
    }

    traject = traject;
  }

  /*
   * rendre la voiture
   */
  public Car[] returnCar() {
    return car;
  }

  /*
   * rendre le type de la trajectoire
   */
  public Traject returnTraject() {
    return traject;
  }

  /*
   * cette fonction va nous permettre de savoir , si la voiture est toujours dans
   * la trajectoire , si nn un message d'alerte sera afficher
   */
  public boolean carInTraject(int i) {
    if (areAccident(i)) {
      return true;
    }
    return traject.isOut(car[i].getCadreNext());

  }

  public Car getCar(int i) {
    return cars[i];
  }

  public Traject getTraject() {
    return traject;
  }

  public boolean areAccident(int i) {
    for (int j = 0; j < 4; j++) {
      if (j != i) {
        if (cars[i].IsCollision(cars[j].getCadreBefore())) {

          // car[i].TryTournerGaucher(1);

          //	if(!traject.isOut(car[i].getCadreNext())){
          //car[i].TournerGaucher(1);
          //	 }else{
          //	 car[i].TournerDroite(1);
          // }
          return true;
        }
      }
    }
    return false;
  }

}
