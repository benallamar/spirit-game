package Observable;

import java.util.LinkedList;

public class Point {

  private int x;
  private int y;

  public Point(int x, int y) {
    x = x;
    y = y;
  }

  public int returnX() {
    return x;
  }

  public int returnY() {
    return y;
  }

  public int getNorme() {
    return (int) Math.sqrt(x * x + y * y);
  }

  public String toString() {
    return x + "/" + y + "\n";
  }

  public Point clone() {
    return new Point(returnX(), returnY());
  }


  public boolean pointInPolygon(LinkedList<Point> polygon) {
    // Vérfier si le point est exactement sur un sommet
    if (pointOnVertex(polygon) == true) {
      return true;
    }

    // Vérifier si le point est dans le polygone ou sur le bord
    int intersections = 0;
    int j = 0;
    int vertices_count = polygon.size();
    Point[] vertices = new Point[vertices_count];
    for (Point point : polygon) {
      vertices[j] = point.clone();
      j++;
    }
    for (int i = 1; i < polygon.size(); i++) {

      Point vertex1 = vertices[i - 1];
      Point vertex2 = vertices[i];
      if (vertex1.returnY() == vertex2.returnY() && vertex1.returnY() == y && x > Math
          .min(vertex1.returnX(), vertex2.returnX()) && x < Math
          .max(vertex1.returnX(), vertex2.returnX())) { // Vérifier si le point est sur un bord horizontal
        return true;
      }
      if (y > Math.min(vertex1.returnY(), vertex2.returnY()) && y <= Math
          .max(vertex1.returnY(), vertex2.returnY()) && x <= Math.max(vertex1.returnX(), vertex2.returnX())
          && vertex1.returnY() != vertex2.returnY()) {
        int xinters =
            (x - vertex1.returnY()) * (vertex2.returnX() - vertex1.returnX()) / (vertex2.returnY() - vertex1
                .returnX()) + vertex1.returnX();
        if (xinters == x) { // Vérifier si le point est sur un bord (autre qu'horizontal)
          return true;
        }
        if (vertex1.returnX() == vertex2.returnX() || x <= xinters) {
          intersections++;
        }
      }
    }
    // Si le nombre de bords par lesquels on passe est impair, le point est dans le polygone.
    if (intersections % 2 != 0) {
      return true;
    } else {
      return false;
    }
  }

  public boolean pointOnVertex(LinkedList<Point> cadre) {

    for (Point point : cadre) {
      if (x == point.returnX() && y == point.returnY()) {
        return true;
      }

    }
    return false;
  }


}
