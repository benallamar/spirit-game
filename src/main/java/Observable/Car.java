package Observable;

import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class Car extends Observable {

  AffineTransform transformationAffine = new AffineTransform();
  private BufferedImage image;
  private int nume;
  private double rotationBefore;
  private double rotationNext;
  private LinkedList<Point> cadreBefore = new LinkedList<Point>();
  private LinkedList<Point> cadreNext = new LinkedList<Point>();
  private int[] positionBefore = new int[2];
  private int[] positionNext = new int[2];
  private float acceleration;
  private float SpeedMax;
  private Point pointBefore;
  private Point pointNext;

  /*
   * return la position de la voiture
   */
  public int[] returnPosition() {
    return positionBefore;
  }

  /*
   * return l'acceleration
   */
  public float returnAccleration() {
    return acceleration;
  }

  /*
   * return la vitesse max
   */
  public float returnSMax() {
    return SpeedMax;
  }

  /*
   * Constructeur de type voiture
   */
  public Car(int[] position, float acceleration, int SpeedMax, BufferedImage image) {

    positionBefore = position;
    positionNext = position;
    acceleration = acceleration;
    SpeedMax = SpeedMax;
    image = image;
    analyzImageNext();
    cadreBefore = cadreNext;
  }

  /*
   * cet fonction va nous permettre de modifier la position de la voiture selon nous envie
   */
  public void setPosition(int x, int y) {
    positionBefore[0] = x;
    positionBefore[1] = y;
  }

  public Car(BufferedImage image, int x, int y, int SpeedMax, float accelration) {

    nume = 1;
    rotationBefore = 0;
    rotationNext = 0;
    positionBefore[0] = x;
    positionBefore[1] = y;
    positionNext[0] = positionBefore[0];
    positionNext[1] = positionBefore[1];
    acceleration = accelration;
    SpeedMax = SpeedMax;
    image = image;
    analyzImageNext();
    pointBefore = pointNext.clone();
    cadreBefore = (LinkedList<Point>) cadreNext.clone();
    transformationAffine.translate(positionBefore[0], positionBefore[1]);
    transformationAffine
        .rotate(rotationBefore, (int) (image.getWidth() / 2), (int) (image.getHeight() / 2));
  }

  public int getName() {
    return nume;
  }


  public void analyzImageNext() {
    cadreNext = new LinkedList<Point>();
    cadreNext.push(getSourceXY((int) (image.getHeight() * 0.4), (int) (image.getWidth() * 0.3)));
    cadreNext.push(getSourceXY(0, (int) (image.getWidth() * 0.4)));
    cadreNext.push(getSourceXY(-(int) (image.getHeight() * 0.4), (int) (image.getWidth() * 0.3)));
    cadreNext.push(getSourceXY(-(int) (image.getHeight() * 0.4), 0));
    cadreNext.push(getSourceXY(-(int) (image.getHeight() * 0.4), -(int) (image.getWidth() * 0.3)));
    cadreNext.push(getSourceXY(0, -image.getWidth() / 2));
    cadreNext.push(getSourceXY((int) (image.getHeight() * 0.4), -(int) (image.getWidth() * 0.3)));
    cadreNext.push(getSourceXY((int) (image.getHeight() * 0.4), 0));
    pointNext = getSourceXY((int) (image.getHeight() * 0.4), image.getWidth() / 4);
  }

  public BufferedImage getImage() {
    return image;
  }

  public LinkedList<Point> getCadreNext() {
    return cadreNext;
  }

  public LinkedList<Point> getCadreBefore() {
    return cadreBefore;
  }

  public int[] getPosition() {
    return positionBefore;
  }

  private Point getSourceXY(int x, int y) {
    int dx = x;
    int dy = y;
    int xRot =
        ((int) (dy * Math.cos(rotationNext) - dx * Math.sin(rotationNext))) + (image.getWidth() / 2)
            + positionNext[0];
    int yRot =
        ((int) (dx * Math.cos(rotationNext) + dy * Math.sin(rotationNext))) + (image.getHeight() / 2)
            + positionNext[1];
    return new Point(xRot, yRot);
  }

  public void reset() {
    pointNext = pointBefore.clone();
    cadreNext = (LinkedList<Point>) cadreBefore.clone();
    positionNext = positionBefore.clone();
    rotationNext = rotationBefore;
  }

  public void TryBougerAvant() {
    int d = 14;
    positionNext[0] = positionBefore[0] + (int) (Math.cos(rotationBefore) * d);
    positionNext[1] = positionBefore[1] + (int) (Math.sin(rotationBefore) * d);
    analyzImageNext();


  }

  public void TryBougerArrier() {
    int d = 14;
    positionNext[0] = positionBefore[0] - (int) (Math.cos(rotationBefore) * d);
    positionNext[1] = positionBefore[1] - (int) (Math.sin(rotationBefore) * d);
    analyzImageNext();

  }

  public void TryTournerDroite(int i) {
    double delta = Math.PI / 20;
    rotationNext = (rotationBefore + i * delta) % (2 * Math.PI);
    analyzImageNext();
  }

  public void TryTournerGaucher(int i) {
    double delta = Math.PI / 20;
    rotationNext = (rotationBefore - i * delta) % (2 * Math.PI);
    analyzImageNext();
  }

  public void BougerAvant() {
    int d = 14;
    positionNext[0] = positionBefore[0] + (int) (Math.cos(rotationBefore) * d);
    positionNext[1] = positionBefore[1] + (int) (Math.sin(rotationBefore) * d);
    analyzImageNext();
    bougerVoiture();

  }

  public void BougerArrier() {
    int d = 14;
    positionNext[0] = positionBefore[0] - (int) (Math.cos(rotationBefore) * d);
    positionNext[1] = positionBefore[1] - (int) (Math.sin(rotationBefore) * d);
    analyzImageNext();
    bougerVoiture();

  }

  public void TournerDroite(int i) {
    double delta = Math.PI / 20;
    rotationNext = (rotationBefore + i * delta) % (2 * Math.PI);
    analyzImageNext();
    bougerVoiture();
  }

  public void TournerGaucher(int i) {
    double delta = Math.PI / 20;
    rotationNext = (rotationBefore - i * delta) % (2 * Math.PI);
    analyzImageNext();
    bougerVoiture();
  }

  public void bougerVoiture() {
    pointBefore = pointNext.clone();
    cadreBefore = (LinkedList<Point>) cadreNext.clone();
    positionBefore = positionNext.clone();
    rotationBefore = rotationNext;
    transformationAffine.setToTranslation(positionBefore[0], positionBefore[1]);
    ;
    transformationAffine.rotate(rotationBefore, image.getWidth() / 2, image.getHeight() / 2);


  }

  public AffineTransform getTrans() {
    return transformationAffine;
  }

  public int[] returnCadreX() {

    int[] cadreX = new int[cadreBefore.size()];
    int i = 0;
    for (Point point : cadreBefore) {
      cadreX[i] = point.returnX();
      i++;
    }
    return cadreX;
  }

  public int[] returnCadreY() {

    int[] cadreY = new int[cadreBefore.size()];
    int i = 0;
    for (Point point : cadreBefore) {
      cadreY[i] = point.returnY();
      i++;
    }
    return cadreY;
  }

  public double getRotationBefore() {
    return rotationBefore;
  }

  public Point getTeteVoiture() {
    return pointNext;
  }

  public boolean IsCollision(LinkedList<Point> cadre) {
    for (Point point : cadreNext) {
      if (point.pointInPolygon(cadre)) {
        return true;
      }
    }
    return false;
  }

}
