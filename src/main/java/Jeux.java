package main;

import Observable.*;
import Observable.Observable;
import Observateur.Observateur;
import Trajectoire.Traject;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;
import javax.imageio.ImageIO;

public class Jeux {

  /**
   * @author BENALLA Marouane &
   */
  public static void loadCars() {
        /* function will load all the data in order to display it */
  }

  public static void main(String[] args) {
    try {

      BufferedImage imgVoiture = ImageIO.read(new File("src/Observable/car2.png"));
      BufferedImage imgPiste = ImageIO.read(new File("src/Trajectoire/piste.png"));

      LinkedList<Car> car = new LinkedList<Car>();
      car.push(new Car(ImageIO.read(new File("src/Observable/car1.png")), 10, 10, 0, 0));
      car.push(new Car(ImageIO.read(new File("src/Observable/car2.png")), 550, 155, 0, 0));
      car.push(new Car(ImageIO.read(new File("src/Observable/car3.png")), 400, 135, 0, 0));
      car.push(new Car(ImageIO.read(new File("src/Observable/car4.png")), 400, 225, 0, 0));

      Traject traject = new Traject(500, 500, imgPiste);
      Observable observable = new Observable(car, traject);

      new Observateur(observable);

    } catch (Exception e) {
      // TODO: handle exception
    }
  }

}
