package Controle;

import Observable.Observable;
import Observateur.Paneau;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class KeyEventControle implements KeyListener {

  private Observable observable;
  private Paneau pan;
  private boolean upButton = false;
  private boolean downButton = false;
  private boolean leftButton = false;
  private boolean rightButton = false;
  private boolean upButtonD = false;
  private boolean downButtonD = false;
  private boolean leftButtonD = false;
  private boolean rightButtonD = false;

  public KeyEventControle(Paneau pan) {
    observable = pan.getObservable();
    pan = pan;
  }

  public Paneau getPan() {
    return pan;
  }

  public void keyPressed(KeyEvent e) {

    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
      rightButton = true;


    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT) {

      leftButton = true;


    }
    if (e.getKeyCode() == KeyEvent.VK_UP) {
      upButton = true;


    }
    if (e.getKeyCode() == KeyEvent.VK_DOWN) {
      downButton = true;

    }
    toDo(0);


  }

  @Override
  public void keyReleased(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
      rightButton = false;


    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT) {

      leftButton = false;


    }
    if (e.getKeyCode() == KeyEvent.VK_UP) {
      upButton = false;


    }
    if (e.getKeyCode() == KeyEvent.VK_DOWN) {
      downButton = false;

    }

    toDo(0);
  }

  @Override
  public void keyTyped(KeyEvent e) {

  }

  public boolean bougerAvant(int i) {

    observable.getCar(i).TryBougerAvant();
    if (!observable.carInTraject(i)) {
      observable.getCar(i).BougerAvant();
      ;
      pan.repaint();
      return true;
    } else {
      observable.getCar(i).reset();
    }
    return false;

  }

  public boolean bougerArrier(int i) {

    observable.getCar(i).TryBougerArrier();
    if (!observable.carInTraject(i)) {
      observable.getCar(i).BougerArrier();
      ;
      pan.repaint();
      return true;
    } else {
      observable.getCar(i).reset();
    }
    return false;

  }

  public boolean tournerDroite(int i, int k) {
    observable.getCar(i).TryTournerDroite(k);
    if (!observable.carInTraject(i)) {
      observable.getCar(i).TournerDroite(k);
      pan.repaint();
      return true;
    } else {
      observable.getCar(i).reset();
    }
    return false;

  }

  public boolean tournerGaucher(int i, int k) {
    observable.getCar(i).TryTournerGaucher(k);
    if (!observable.carInTraject(i)) {
      observable.getCar(i).TournerGaucher(k);
      pan.repaint();
      return true;
    } else {
      observable.getCar(i).reset();
    }
    return false;
  }

  public void toDo(int i) {
    if (downButton) {
      bougerArrier(i);
    }
    if (upButton) {
      bougerAvant(i);
    }
    if (rightButton) {
      tournerDroite(i, 1);
    }
    if (leftButton) {
      tournerGaucher(i, 1);
    }
  }

  public void essayBouger(int i) {

    switch (observable.getTraject().extremiste(observable.getCar(i).getTeteVoiture().returnX(),
        observable.getCar(i).getTeteVoiture().returnY(), observable.getCar(i).getRotationBefore())) {
      case 0:
        tournerDroite(i, 1);
        break;
      case 1:
        tournerDroite(i, 1);
        break;
      case 2:
        tournerDroite(i, 1);
        break;
      case 3:
        break;
      case 4:
        tournerDroite(i, 1);
        break;
      case 5:
        tournerDroite(i, 1);
        break;
      case 6:
        tournerDroite(i, 1);
        break;
    }

    if (!bougerAvant(i)) {
      bougerArrier(i);
    }


  }

}